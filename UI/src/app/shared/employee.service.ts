import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Employee } from './employee.model';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  selectedEmployee : Employee;
  employees : Employee[];
  
  
  readonly baseURL = 'http://localhost:3000/employees';
 
 
 
  constructor(private http : HttpClient ) {  
    
     
  }


    postEmployee(empData : Employee){
     return this.http.post(this.baseURL, empData);
    }

    getEmployeeList(){
      return this.http.get(this.baseURL);
    }

    putEmployee(empputData : Employee){
      console.log(empputData);
      return this.http.put(this.baseURL + `/${empputData.id}`, empputData);
    }

    deleteEmployee(_id:string){
      return this.http.delete(this.baseURL + `/${_id}`);
    }

    onprinthello(){
      console.log("helloworld","helllllllllll");
    }
}
