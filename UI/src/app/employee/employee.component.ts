import { Component, OnInit } from '@angular/core';

import { EmployeeService } from '../shared/employee.service';
import { NgForm} from '@angular/forms';
import { Employee } from '../shared/employee.model';
import { from } from 'rxjs';
import { EmbeddedTemplateAst } from '@angular/compiler';


 declare var M:any;


 @Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
  providers: [EmployeeService]
})
export class EmployeeComponent implements OnInit {

  // fullName: any;
  // address : any;

  constructor(private employeeService : EmployeeService ) { }

  ngOnInit() {
    this.resetForm();
    this.getingEmployeeList();  
  }

  resetForm(form? : NgForm){
    if(form)
    form.reset();
    this.employeeService.selectedEmployee = {
    id:"",
    firstName: "",
    lastName:  "", 
    desgination:"",
    emailId:"",
    phoneNo:null,
    addressLine1:"",  
    addressLine2:"",
    addressLine3:"",
    city:"",
    state:"",
    pincode:null
    }
  }

onSubmit(form : NgForm){
  
  if(form.value.id == ""){
    this.employeeService.postEmployee(form.value).subscribe((res) => {
    this.resetForm(form);
    this.getingEmployeeList();
    M.toast({html: 'Saved Sucessfully', classes :'rounded' });
  });
}

  else{
    console.log(form);
    this.employeeService.putEmployee(form.value).subscribe((res) => {
      this.resetForm(form);
      this.getingEmployeeList();
      M.toast({html: 'Updated Sucessfully', classes :'rounded' });
    });
  }}

  getingEmployeeList(){
    this.employeeService.getEmployeeList().subscribe((res) => {
      this.employeeService.employees = res as Employee[];
      console.log(this.employeeService.employees,"aaa")
      // this.employeeService.employees.forEach(el => {
      //   el.fullName = el.FirstName + el.LastName;
      //   el.address = el.AddressLine1 + el.AddressLine2 + el.AddressLine3;
       
      // });
      // console.log( this.employeeService.employees,"names")
    });
  }

    onEdit(empData: Employee) {
      console.log(empData);
      this.employeeService.getEmployeeList().subscribe((res) => {
        this.employeeService.employees = res as Employee[];
      });
      // this.employeeService.selectedEmployee = {
      //   id:empData._id,
      //   firstName: empData.FirstName,
      //   lastName:  empData.LastName, 
      //   desgination:empData.Desgination,
      //   emailId:empData.Emailid,
      //   phoneNo:empData.Phoneno,
      //   addressLine1:empData.AddressLine1,  
      //   addressLine2:empData.AddressLine2,
      //   addressLine3:empData.AddressLine3,
      //   city:empData.city,
      //   state:empData.State,
      //   pincode:empData.Pincode
      //   }
    }

    // OnDelete(id: string,form : NgForm){
    //   if(confirm('want to del the record')==true){
    //     this.employeeService.deleteEmployee(id).subscribe((res)=>{
    //       this.getingEmployeeList();
    //       this.resetForm(form);
    //       M.toast({html: 'Deleted Sucessfully', classes :'rounded' });
    //     });
    //   }
    // }
  
    onDelete(_id: string, form: NgForm) {
      console.log(_id,"id");
      console.log(form,"form");
      if (confirm('want to del the record') == true) {
        this.employeeService.deleteEmployee(_id).subscribe((res) => {
          this.getingEmployeeList();
          this.resetForm(form);
          M.toast({ html: 'Deleted successfully', classes: 'rounded' });
        });
      }
    }

}
