const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/CrudDb', (err) => {
    if(!err){
        console.log("MongoDB connection sucessed..")
    }
    else
        console.log("MongoDb error in connection : " + JSON.stringify(err,undefined,1));
});

module.exports = mongoose;